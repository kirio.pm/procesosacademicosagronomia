$.validator.addMethod( "datetimeISO", function( value, element ) {
    return this.optional( element ) || /^\d{4}[-]\d{1,2}[-]\d{1,2}[ ]([01]\d|2[0-3]|[0-9])(:[0-5]\d){1,2}$/.test( value );
}, "Ingrese una fecha en formato AAAA-MM-DD hh:mm" );

