$.validator.addMethod( "integer", function( value, element ) {
    return this.optional( element ) || /^-?\d+$/.test( value );
}, "Solo números positivos o negativos, no usar decimales" );