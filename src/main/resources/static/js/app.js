$(document).ready(function(){

	//Desabilita el comportamiento por defecto de las etiquetas <li> cuando estas tienen en su src = "#"
	//Para desabilitar este comportamiento agregar class = "disabled_href"
    $('.disabled_href').click(function(e){
        e.preventDefault();
    });
});
/*
 * Token and header for post ajax request
 */
var token = $("meta[name='_csrf']").attr("content");
var header = $("meta[name='_csrf_header']").attr("content");

 $(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(header, token);
    });

function showSnackbar(message,time,html){
	$.snackbar({
		content: message,
		timeout: time,
		htmlAllowed: html
	});
}

function showToast(message,time,html){
	$.snackbar({
		content: message,
		timeout: time,
		style: "toast",
		htmlAllowed: html
	});
}

