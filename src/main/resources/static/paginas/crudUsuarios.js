let saveForm = $("#save-form-usuario");
let modifyForm = $("#update-form-usuario");
let changePass = $("#modalForm");
$(document).ready(function () {
    var dataTable = $('#dataTable').DataTable({
        "bDestroy": true,
        "ajax": {
            "url": '/api/usuarios/getUsuarios',
            "type": "GET"
        },
        "language": {
            "url": "/vendor/datatables/Spanish.json"
        },
        "columns": [
            {"data": "username"},
            {"data": "joiningDate"},
            {"data": "lastLogin"},
            {"data": "cargo"},
            {"data": "roles", render: "[, ].tipoRol"},
            {"data": ''}
        ],
        "columnDefs": [{
                "className": "text-center",
                "targets": -1,
                "data": {"data": "id"},
                "defaultContent": '<button class="btn btn-warning btn-circle btn-edit" data-toggle="tooltip" data-placement="right" title="Editar Usuario"><i class="fas fa-edit"></i></button> ' +
                        '<button class="btn btn-danger btn-circle btn-del" data-toggle="tooltip" data-placement="right" title="Eliminar Usuario"><i class="fas fa-trash"></i></button>' +
                        '<button class="btn btn-info btn-circle  btn-contacts" data-toggle="tooltip" data-placement="right" title="Cambiar contraseña"><i class="fas fa-user-shield"></i></button>'
            }
        ]
    });
    //Modal cambiar contraseña
    $('#dataTable tbody').on('click', '.btn-contacts', function () {
        let row = dataTable.row($(this).parents('tr')).data();
        $("#nombreUsuario").val(row['username']);
        $('#modal-cambiar-pass').modal('show');
    });
    //Modal guardar                    
    $(document).on("click", "#nuevo-btn", function () {
        var rolesSelect = document.getElementById("rolesSelect");
        var rolesSeleccionado = [];
        for (var i = 0; i < rolesSelect.length; i++) {
            if (rolesSelect.options[i].selected) {
                var roles = {
                    "id": rolesSelect.options[i].value
                };
                rolesSeleccionado.push(roles);
            }
        }
        if (saveForm.valid()) {
            url = "/api/usuarios/guardar";
            type = "POST";
            data = {
                username: $("#username").val().trim(),
                password: $("#password").val().trim(),
                nombre: $("#nombre").val().trim(),
                apellido: $("#apellido").val().trim(),                                
                correo: $("#correo").val().trim(),
                cargo: $("#cargo").val().trim(),
                roles: rolesSeleccionado
            };
            enviarDatos(url, type, data);
            $('#modal-nuevo-usuario').modal('hide');
        }
    });
    // end modal guardar

    //Modal editar
    $('#dataTable tbody').on('click', '.btn-edit', function () {
        var selectRef = $("#rolesSelectEdit");
        var url = "/api/usuarios/roles";
        let row = dataTable.row($(this).parents('tr')).data();
        $("#usernameEdit").val(row['username']);
        $("#nombreEdit").val(row['nombre']);
        $("#apellidoEdit").val(row['apellido']);
        $("#cargo").val(row['cargo']);
        $("#correoEdit").val(row['correo']);
        buildOptionListRoles(url, selectRef, row['roles']);
        $("#idUsuario").val(row['id']);
        $('#modal-editar-usuario').modal('show');
    });
    $(document).on("click", "#editar-btn", function (event) {

        var rolesSelect = document.getElementById("rolesSelectEdit");
        var rolesSeleccionado = [];
        for (var i = 0; i < rolesSelect.length; i++) {
            if (rolesSelect.options[i].selected)
            {
                var roles = {
                    "id": rolesSelect.options[i].value
                };
                rolesSeleccionado.push(roles);
            }
        }
        if (modifyForm.valid()) {
            var idUsuario = $("#idUsuario").val().trim();
            url = "/api/usuarios/editar/" + idUsuario;
            type = "PUT";
            data = {
                username: $("#usernameEdit").val().trim(),
                nombre: $("#nombreEdit").val().trim(),
                apellido: $("#apellidoEdit").val().trim(),                                
                correo: $("#correoEdit").val().trim(),
                cargo: $("#cargoEdit").val().trim(),                            
                roles: rolesSeleccionado
            };
            enviarDatos(url, type, data);
            $('#modal-editar-usuario').modal('hide');
        }
    });
    // end modal guardar
    // Eliminar empleado
    $('#dataTable tbody').on('click', '.btn-del', function () {                        
        let row = dataTable.row($(this).parents('tr')).data();
        var idUsuario=row['id'];
        var url = "/api/usuarios/eliminar/"+idUsuario;
        var type = "DELETE";
        data = {};
        confirmarEliminacion(url, type, data);
    });
    $(document).on("click", "#btn-save-pass", function (event) {
        if (changePass.valid()) {
            url = "/api/usuarios/changePass";
            type = "POST";
            data = {
                user: $("#nombreUsuario").val().trim(),
                pass: $("#passAntigua").val().trim(),
                passConfirmed: $("#passModificadaConfirmacion").val().trim()

            };
            enviarDatos(url, type, data);
            $('#modal-cambiar-pass').modal('hide');
        }
    });
});
saveForm.validate({
    errorClass: 'text-danger',
    rules: {
        username: {
            required: true,
            minlength: 8,
            maxlength: 15
        },
         nombre: {
            required: true,
            minlength: 1,
            maxlength: 25
        },
         apellido: {
            required: true,
            minlength: 1,
            maxlength: 25
        },
        password: {
            required: true,
            minlength: 8,
            maxlength: 60
        },
        correo: {
            required: true
        },
        cargo: {
            required:true
        }

    }
});
modifyForm.validate({
    errorClass: 'text-danger',
    rules: {
        usernameEdit: {
            required: true,
            minlength: 8,
            maxlength: 15
        },
        nombreEdit: {
            required: true,
            minlength: 1,
            maxlength: 25
        },
         apellidoEdit: {
            required: true,
            minlength: 1,
            maxlength: 25
        },
        correoEdit: {
            required: true
        },
        cargoEdit: {
           required: true
        }
    }
});
changePass.validate({
    errorClass: 'text-danger',
    rules: {
        passModificada: {
            required: true,
            minlength: 8,
            maxlength: 60
        },
        passModificadaConfirmacion: {
            required: true,
            minlength: 8,
            equalTo: "#passModificada"
        },
        passAntigua: {
            required: true,
            minlength: 1,
            remote: {
                url: "/api/usuarios/password",
                type: "GET",
                data: {
                    password: function () {
                        return $("#passAntigua").val();
                    },
                    username: function () {
                        return $("#nombreUsuario").val();
                    }
                }
            }			
        }
    }

});

function confirmarEliminacion(url, type, data) {
    event.preventDefault();
    Swal.fire({
        icon: 'warning',
        title: "Eliminar",
        text: "Esta seguro de eliminar este registro?",
        showCancelButton: true,
        confirmButtonText: "Si",
        cancelButtonText: "No, Cancelar",
        showConfirmButton: true
    }).then((result) => {
        if (result.value) {
            enviarDatos(url, type, data);
        }
    }
    );
 }
function enviarDatos(url, type, data) {
    $.ajax({
        url: url,
        type: type,
        data: JSON.stringify(data),
        contentType: "application/json",
        dataType: 'json',
        timeout: 60000
    }).done(function (response) {
        if (response.HTTP === 200) {
            Swal.fire({
                icon: 'success',
                title: response.mensaje,
                showConfirmButton: true
            });
            $("#dataTable").DataTable().ajax.reload();
        } else if (response.HTTP === 500) {
            Swal.fire({
                icon: 'error',
                title: response.error,
                text: response.mensaje,
                showConfirmButton: true
            });
        } else {

        }
    }).fail(function (xhr, status, error) {
        console.log("No se completo la peticion");
    }).always(function () {
        document.getElementById("save-form-usuario").reset();
        document.getElementById("update-form-usuario").reset();
        document.getElementById("modalForm").reset();
        data = undefined;
    });
 }
function buildOptionListRoles(url, selectRef, roles) {
    var result = getRequest(url);
    var rolesOrdenados = roles.sort(function (a, b) {
        if (a.id > b.id) {
            return 1;
        }
        if (a.id < b.id) {
            return -1;
        }        
        return 0;
    });
    result.done(function (data) {
        var options = "";
        var i = 0;
        $.each(data.data, function (key, item) {
            if (item.id === rolesOrdenados.map(obj => obj.id)[i]) {
                options += "<option selected value='" + item.id + "'>" + item.tipoRol + "</option>";
                i++;
            } else
                options += "<option value='" + item.id + "'>" + item.tipoRol + "</option>";
        });
        selectRef.html(options);
    });

}
function getRequest(url, data, successMessage) {
    return $.get({
        url: url,
        data: data
    }).done(function (data) {
    }).fail(function (xhr, status, errorThrown) {
		$.snackbar({content: "Ha ocurrido un error " + xhr.status +":" + errorThrown + "  Estado: " + status});
    });
}