let saveForm = $("#save-form-jefatura");
let modifyForm = $("#update-form-jefatura");
$(document).ready(function () {
    var dataTable = $('#dataTable').DataTable({
        "bDestroy": true,
        "ajax": {
            "url": '/api/Jefatura/todos',
            "type": "GET"
        },
        "language": {
            "url": "/vendor/datatables/Spanish.json"
        },
        "columns": [
            {"data": "nombre"},            
            {"data": ''}
        ],
        "columnDefs": [{
                "className": "text-center",
                "targets": -1,
                "data": {"data": "id"},
                "defaultContent": '<button class="btn btn-warning btn-circle btn-edit" data-toggle="tooltip" data-placement="right" title="Editar Usuario"><i class="fas fa-edit"></i></button> ' +
                        '<button class="btn btn-danger btn-circle btn-del" data-toggle="tooltip" data-placement="right" title="Eliminar Usuario"><i class="fas fa-trash"></i></button>'
                        
            }
        ]
    });
   
    //Modal guardar                    
    $(document).on("click", "#nuevo-btn", function () {        
        if (saveForm.valid()) {
            url = "/api/Jefatura/guardar";
            type = "POST";
            data = {
                nombre: $("#nombre").val().trim(),                
            };
            enviarDatos(url, type, data);
            $('#modal-nuevo-jefatura').modal('hide');
        }
    });
    // end modal guardar

    //Modal editar
    $('#dataTable tbody').on('click', '.btn-edit', function () {       
        let row = dataTable.row($(this).parents('tr')).data();       
        $("#nombreEdit").val(row['nombre']);      
        $("#idJefatura").val(row['id']);
        $('#modal-editar-jefatura').modal('show');
    });
	//boton de actualizar
    $(document).on("click", "#editar-btn", function (event) {
        if (modifyForm.valid()) {
            var idJefatura = $("#idJefatura").val().trim();
            url = "/api/Jefatura/editar/" + idJefatura;
            type = "PUT";
            data = {                
                nombre: $("#nombreEdit").val().trim(),                
            };
            enviarDatos(url, type, data);
            $('#modal-editar-jefatura').modal('hide');
        }
    });
    // end modal guardar
    // Eliminar
    $('#dataTable tbody').on('click', '.btn-del', function () {                        
        let row = dataTable.row($(this).parents('tr')).data();
        var idJefatura=row['id'];
        var url = "/api/Jefatura/eliminar/"+idJefatura;
        var type = "DELETE";
        data = {};
        confirmarEliminacion(url, type, data);
    });
});
saveForm.validate({
    errorClass: 'text-danger',
    rules: {
         nombre: {
            required: true,
            minlength: 1,
            maxlength: 25
        }
    }
});
modifyForm.validate({
    errorClass: 'text-danger',
    rules: {
        nombreEdit: {
            required: true,
            minlength: 1,
            maxlength: 25
        }
    }
});
function confirmarEliminacion(url, type, data) {
    event.preventDefault();
    Swal.fire({
        icon: 'warning',
        title: "Eliminar",
        text: "Esta seguro de eliminar este registro?",
        showCancelButton: true,
        confirmButtonText: "Si",
        cancelButtonText: "No, Cancelar",
        showConfirmButton: true
    }).then((result) => {
        if (result.value) {
            enviarDatos(url, type, data);
        }
    }
    );
 }
function enviarDatos(url, type, data) {
    $.ajax({
        url: url,
        type: type,
        data: JSON.stringify(data),
        contentType: "application/json",
        dataType: 'json',
        timeout: 60000
    }).done(function (response) {
        if (response.HTTP === 200) {
            Swal.fire({
                icon: 'success',
                title: response.mensaje,
                showConfirmButton: true
            });
            $("#dataTable").DataTable().ajax.reload();
        } else if (response.HTTP === 500) {
            Swal.fire({
                icon: 'error',
                title: response.error,
                text: response.mensaje,
                showConfirmButton: true
            });
        } else {

        }
    }).fail(function (xhr, status, error) {
        console.log("No se completo la peticion");
    }).always(function () {
        document.getElementById("save-form-jefatura").reset();
        document.getElementById("update-form-jefatura").reset();               
    });
 }