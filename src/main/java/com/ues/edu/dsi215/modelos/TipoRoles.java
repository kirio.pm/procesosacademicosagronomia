package com.ues.edu.dsi215.modelos;

import java.io.Serializable;

public enum TipoRoles implements Serializable {

	USUARIO("USUARIO"), VENTANILLA("VENTANILLA"), ADMINISTRADOR("ADMINISTRADOR"), JEFE("JEFE"), AUXILIAR("AUXILIAR");

	String tipoRol;

	private TipoRoles(String tipoRol) {
		this.tipoRol = tipoRol;
	}

	public String getTipoRol() {
		return tipoRol;
	}
}
