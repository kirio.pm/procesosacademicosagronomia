package com.ues.edu.dsi215.modelos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "Materia")
public class Materia {

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    
	@Column(length = 15)
    private String codigo;
	
    @Column(length = 50)
    private String nombre;
    
    @Column(length = 15)
    private String tipoMateria;
    
    @ManyToOne(fetch = FetchType.LAZY)
    private Departamento departamento;
    
}
