package com.ues.edu.dsi215.modelos;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "Roles")
public class Roles {

	 @Id
	    @GeneratedValue(strategy = GenerationType.SEQUENCE)
	    private Long id;

	    @Column(name="tipo_rol", length=30, unique=true, nullable=false)
	    private String tipoRol = TipoRoles.USUARIO.getTipoRol();

	    @ManyToMany(mappedBy = "roles")
	    private List<Usuarios> usuarios = new ArrayList<>();
}
