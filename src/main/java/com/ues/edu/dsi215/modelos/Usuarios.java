package com.ues.edu.dsi215.modelos;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "Usuarios")
public class Usuarios {

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(length = 50, unique = true)
    private String username;

    @Column(length = 200)
    private String password;
    
    @Column(length = 50)
    private String nombre;

    @Column(length = 50)
    private String apellido;

    @Column(length = 50)
    private String cargo;

    @Column(length = 50)
    private String correo;

    @Column
    private LocalDate joiningDate;

    @Column
    private LocalDate lastLogin;
    
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "usuarios_roles",
            joinColumns = {
                @JoinColumn(name = "id_usuarios")},
            inverseJoinColumns = {
                @JoinColumn(name = "id_roles")})   
    private List<Roles> roles = new ArrayList<>();
    
    
}
