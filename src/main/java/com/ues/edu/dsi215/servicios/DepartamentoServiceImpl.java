package com.ues.edu.dsi215.servicios;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.ues.edu.dsi215.modelos.Departamento;
import com.ues.edu.dsi215.repositorios.DepartamentoRepository;

@Service
public class DepartamentoServiceImpl implements DepartamentoService{
	
	@Autowired
	private DepartamentoRepository repository;

	@Override
	public List<Departamento> obtenerTodo() {	
		return repository.findAll();
	}

	@Override
	public Departamento guardar(Departamento model) {		
		return repository.save(model);
	}

	@Override
	public Departamento editar(Departamento model, Long id) {		
		return repository.findById(id).map(dep->{
			dep.setNombre(model.getNombre());
			dep.setJefatura(model.getJefatura());
			return repository.save(dep);
		}).orElseGet(() -> {
			model.setId(id);
			return repository.save(model);
		});
	}

	@Override
	public Departamento buscar(Long id) {
		Optional<Departamento> optional = repository.findById(id);
		return optional.orElseThrow(() -> new EmptyResultDataAccessException("No Encontrado", 500));
	}

	@Override
	public Departamento findByNombre(String nombre) {		
		return repository.findByNombre(nombre);
	}

	@Override
	public void eliminar(Long id) {
		if (!repository.existsById(id)) {
			throw new EmptyResultDataAccessException("No Encontrado", 500);
		}
		repository.deleteById(id);
		
	}

}
