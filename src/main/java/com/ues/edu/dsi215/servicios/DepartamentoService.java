package com.ues.edu.dsi215.servicios;

import java.util.List;

import com.ues.edu.dsi215.modelos.Departamento;

public interface DepartamentoService {

	public List<Departamento> obtenerTodo();
    public Departamento guardar(Departamento model);
    public Departamento editar(Departamento model, Long id);
    public Departamento buscar(Long id);
    public Departamento findByNombre(String nombre);
    public void eliminar(Long id);
}
