package com.ues.edu.dsi215.servicios;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.ues.edu.dsi215.modelos.Jefatura;
import com.ues.edu.dsi215.modelos.Materia;
import com.ues.edu.dsi215.repositorios.JefaturaRepository;

@Service
public class JefaturaServiceImpl implements JefaturaService {

	@Autowired
	private JefaturaRepository repository;

	@Override
	public List<Jefatura> obtenerTodo() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}

	@Override
	public Jefatura guardar(Jefatura model) {
		
		return repository.save(model);
	}

	@Override
	public Jefatura editar(Jefatura model, Long id) {
		
		return repository.findById(id).map(jefatura->{
			jefatura.setNombre(model.getNombre());			
			return repository.save(jefatura);
		}).orElseGet(() -> {
			model.setId(id);
			return repository.save(model);
		});
	}

	@Override
	public Jefatura buscar(Long id) {
		Optional<Jefatura> optional = repository.findById(id);
		return optional.orElseThrow(() -> new EmptyResultDataAccessException("No Encontrado", 500));
	}

	@Override
	public void eliminar(Long id) {
		if (!repository.existsById(id)) {
			throw new EmptyResultDataAccessException("No Encontrado", 500);
		}
		repository.deleteById(id);
		
	}
}
