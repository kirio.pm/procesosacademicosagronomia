package com.ues.edu.dsi215.servicios;

import java.util.List;

import com.ues.edu.dsi215.modelos.Materia;

public interface MateriaService {

	public List<Materia> obtenerTodo();
    public Materia guardar(Materia model);
    public Materia editar(Materia model, Long id);
    public Materia buscar(Long id);
    public Materia findByNombre(String nombre);
    public void eliminar(Long id);
}
