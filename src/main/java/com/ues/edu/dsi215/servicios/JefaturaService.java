package com.ues.edu.dsi215.servicios;

import java.util.List;

import com.ues.edu.dsi215.modelos.Jefatura;



public interface JefaturaService {

	public List<Jefatura> obtenerTodo();
    public Jefatura guardar(Jefatura model);
    public Jefatura editar(Jefatura model, Long id);
    public Jefatura buscar(Long id);
    public void eliminar(Long id);
}
