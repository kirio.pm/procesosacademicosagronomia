package com.ues.edu.dsi215.servicios;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.ues.edu.dsi215.modelos.Usuarios;
import com.ues.edu.dsi215.repositorios.UsuariosRepository;

@Service
public class UsuariosServiceImpl implements UsuariosService {

	@Autowired
	private UsuariosRepository repository;

	@Autowired
	private PasswordEncoder enc;

	@Override
	public List<Usuarios> obtenerTodo() {
		return repository.findAll();
	}

	@Override
	public Usuarios guardar(Usuarios model) {
		model.setJoiningDate(LocalDate.now());
		model.setLastLogin(LocalDate.now());
		encryptUserPassword(model);
		return repository.save(model);
	}

	@Override
	public Usuarios editar(Usuarios model, Long id) {
		return repository.findById(id).map(usuario -> {
			usuario.setUsername(model.getUsername());
			usuario.setNombre(model.getNombre());
			usuario.setApellido(model.getApellido());
			usuario.setCargo(model.getCargo());
			usuario.setCorreo(model.getCorreo());
			usuario.setRoles(model.getRoles());
			return repository.save(usuario);
		}).orElseGet(() -> {
			model.setId(id);
			return repository.save(model);
		});
	}

	@Override
	public Usuarios buscar(Long id) {
		Optional<Usuarios> optional = repository.findById(id);
		return optional.orElseThrow(() -> new EmptyResultDataAccessException("No Encontrado", 500));
	}

	@Override
	public Usuarios findByUsername(String username) {
		return repository.findByUsername(username);
	}

	@Override
	public void eliminar(Long id) {
		if (!repository.existsById(id)) {
			throw new EmptyResultDataAccessException("No Encontrado", 500);
		}
		repository.deleteById(id);
	}

	@Override
	public void updateUserLastLogging(Long id) {
		if (!repository.existsById(id)) {
			throw new EmptyResultDataAccessException("No Encontrado", 500);
		} else {
			Usuarios usuarioLogeado = repository.findById(id).get();
			usuarioLogeado.setLastLogin(LocalDate.now());
			repository.save(usuarioLogeado);
		}

	}

	@Override
	public Usuarios findByCorreo(String email) {
		 return repository.findByCorreo(email);
	}

	@Override
	public Boolean isValidPassword(String username, String pass) {
		return enc.matches(pass, repository.findByUsername(username).getPassword());
	}

	@Override
	public void updatePassword(String password, Long userId) {
		 repository.updatePassword(password, userId);
		
	}

	@Override
	public void updatePasswordPersonal(String username, String pass, String new_pass) {
		Usuarios usuario = repository.findByUsername(username);
        if (enc.matches(pass, usuario.getPassword())) {
            usuario.setPassword(enc.encode(new_pass));
            repository.save(usuario);
        }
		
	}
	public void encryptUserPassword(Usuarios usuario) {
		usuario.setPassword(enc.encode(usuario.getPassword()));
	}

	public void updateUserPasswordIfNotEquals(Usuarios usuario) {
		String currentUserPassword = repository.findById(usuario.getId()).get().getPassword();
		if (!usuario.getPassword().equals(currentUserPassword)) {
			usuario.setPassword(enc.encode(usuario.getPassword()));
		}
	}

}
