package com.ues.edu.dsi215.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.ues.edu.dsi215.modelos.Roles;
import com.ues.edu.dsi215.repositorios.RolesRepository;

@Service
public class RolesServiceImpl implements RolesService {
	@Autowired
	private RolesRepository repository;

	@Override
	public List<Roles> obtenerTodo() {	
		return repository.findAll(Sort.by(Sort.Direction.ASC, "id"));
	}

}
