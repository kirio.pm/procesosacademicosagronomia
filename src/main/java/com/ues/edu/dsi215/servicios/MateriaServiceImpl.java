package com.ues.edu.dsi215.servicios;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.ues.edu.dsi215.modelos.Materia;
import com.ues.edu.dsi215.repositorios.MateriaRepository;

@Service
public class MateriaServiceImpl implements MateriaService{
	@Autowired
	private MateriaRepository repository;

	@Override
	public List<Materia> obtenerTodo() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}

	@Override
	public Materia guardar(Materia model) {
		// TODO Auto-generated method stub
		return repository.save(model);
	}

	@Override
	public Materia editar(Materia model, Long id) {
		// TODO Auto-generated method stub
		return repository.findById(id).map(materia->{
			materia.setCodigo(model.getCodigo());
			materia.setNombre(model.getNombre());
			materia.setDepartamento(model.getDepartamento());
			return repository.save(materia);
		}).orElseGet(() -> {
			model.setId(id);
			return repository.save(model);
		});
	}

	@Override
	public Materia buscar(Long id) {
		// TODO Auto-generated method stub
		Optional<Materia> optional = repository.findById(id);
		return optional.orElseThrow(() -> new EmptyResultDataAccessException("No Encontrado", 500));
	}

	@Override
	public Materia findByNombre(String nombre) {
		// TODO Auto-generated method stub
		return repository.findByNombre(nombre);
	}

	@Override
	public void eliminar(Long id) {
		if (!repository.existsById(id)) {
			throw new EmptyResultDataAccessException("No Encontrado", 500);
		}
		repository.deleteById(id);
		
	}

	
}
