package com.ues.edu.dsi215.servicios;

import java.util.List;

import com.ues.edu.dsi215.modelos.Usuarios;



public interface UsuariosService {
	 	public List<Usuarios> obtenerTodo();
	    public Usuarios guardar(Usuarios model);
	    public Usuarios editar(Usuarios model, Long id);
	    public Usuarios buscar(Long id);
	    public Usuarios findByUsername(String username);
	    public void eliminar(Long id);
	    public void updateUserLastLogging(Long id);
	    Usuarios findByCorreo(String email);    
	    Boolean isValidPassword(String username, String pass);
	    void updatePassword(String password, Long userId);
	    void updatePasswordPersonal(String username, String pass, String new_pass);
}
