package com.ues.edu.dsi215.rest;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ues.edu.dsi215.modelos.Materia;
import com.ues.edu.dsi215.servicios.MateriaService;

@RestController
public class MateriaRestController {

	@Autowired
	private MateriaService service;
	
	@GetMapping("/api/Materia/todos")
    public Map index() {
        Map<String, Object> response = new HashMap<>();
        response.put("data", service.obtenerTodo());
        return response;
    }

    @PostMapping("/api/Materia/guardar")
    public Map guardar(@Valid @RequestBody Materia materia, Errors errors) {
        Map<String, Object> response = new HashMap<>();
        if(errors.hasErrors()) {
        	String results= errors.getAllErrors()
        			.stream()
        			.map(error->error.getDefaultMessage())
        			.collect(Collectors.joining(", "));
        	response.put("HTTP", HttpURLConnection.HTTP_BAD_REQUEST);
            response.put("mensaje", "Error de validacion");
            response.put("error", results);
        }
        else {
	        try {
	        	
	            service.guardar(materia);
	            response.put("HTTP", HttpURLConnection.HTTP_OK);
	            response.put("mensaje", "La Materia :".concat(materia.getNombre()).concat(" Ha sido guardada correctamente"));
	        } catch (DataAccessException e) {
	            response.put("HTTP", HttpURLConnection.HTTP_INTERNAL_ERROR);
	            response.put("mensaje", "Error de validacion");
	            response.put("error", e.getMostSpecificCause().getMessage());
	        }
        }
        return response;
        
       
    }

    @PutMapping("/api/Materia/editar/{id}")
    public Map actualizar(@Valid @RequestBody Materia materia, @PathVariable Long id) {
        Map<String, Object> response = new HashMap<>();
        try {
            service.editar(materia, id);
            response.put("HTTP", HttpURLConnection.HTTP_OK);
            response.put("mensaje", "La Materia :".concat(materia.getNombre()).concat(" Ha sido actualizada correctamente"));
        } catch (DataAccessException e) {
            response.put("HTTP", HttpURLConnection.HTTP_INTERNAL_ERROR);
            response.put("mensaje", "Error de validacion");
            response.put("error", e.getMostSpecificCause().getMessage());
        }
        return response;
    }
    @DeleteMapping("/api/Materia/eliminar/{id}")
    public Map eliminar(@PathVariable Long id){
        Map<String, Object> response = new HashMap<>();
        try {
            service.eliminar(id);
             response.put("HTTP", HttpURLConnection.HTTP_OK);
            response.put("mensaje", " Materia eliminada exitosamente");
        } catch (DataAccessException e) {
            response.put("HTTP", HttpURLConnection.HTTP_INTERNAL_ERROR);
            response.put("mensaje", "Error de validacion");
            response.put("error", e.getMostSpecificCause().getMessage());
        }
        return response;
    }
}
