package com.ues.edu.dsi215.rest;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ues.edu.dsi215.dto.DatatableWrapper;
import com.ues.edu.dsi215.dto.PassFormDTO;
import com.ues.edu.dsi215.dto.RolDTO;
import com.ues.edu.dsi215.dto.UsuarioDTO;
import com.ues.edu.dsi215.modelos.Roles;
import com.ues.edu.dsi215.modelos.Usuarios;
import com.ues.edu.dsi215.servicios.RolesService;
import com.ues.edu.dsi215.servicios.UsuariosService;

@RestController
public class UsuariosRestController {

	@Autowired
	private UsuariosService usuarioService;

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private RolesService rolService;

	/*@Autowired
	private MessageSource messageSource;*/

	@GetMapping("/api/usuarios/getUsuarios")
	public DatatableWrapper<UsuarioDTO> todos() {
		return new DatatableWrapper<>(
				usuarioService.obtenerTodo().stream().map(this::convertToDTO).collect(Collectors.toList()));
	}

	@PostMapping(path = "/api/usuarios/guardar")
	public Map save(@RequestBody Usuarios usuario) {
		Map<String, Object> response = new HashMap<>();
		try {
			usuarioService.guardar(usuario);
			response.put("HTTP", HttpURLConnection.HTTP_OK);
			response.put("mensaje", " almacenado exitosamente con nombre:".concat(" ").concat(usuario.getUsername()));
		} catch (DataAccessException e) {
			response.put("HTTP", HttpURLConnection.HTTP_INTERNAL_ERROR);
			response.put("mensaje", "Error de validacion");
			response.put("error", e.getMostSpecificCause().getMessage());
		}
		return response;
	}

	@PutMapping(value = "/api/usuarios/editar/{id}")
	public Map edit(@RequestBody Usuarios usuario, @PathVariable Long id) {
		Map<String, Object> response = new HashMap<>();
		try {
			usuarioService.editar(usuario, id);
			response.put("HTTP", HttpURLConnection.HTTP_OK);
			response.put("mensaje", " Usuario modificado exitosamente");
		} catch (DataAccessException e) {
			response.put("HTTP", HttpURLConnection.HTTP_INTERNAL_ERROR);
			response.put("mensaje", "Error de validacion");
			response.put("error", e.getMostSpecificCause().getMessage());
		}
		return response;
	}

	@DeleteMapping("/api/usuarios/eliminar/{id}")
	public Map delete(@PathVariable Long id) {
		Map<String, Object> response = new HashMap<>();
		try {
			usuarioService.eliminar(id);
			response.put("HTTP", HttpURLConnection.HTTP_OK);
			response.put("mensaje", " Usuario eliminado exitosamente");
		} catch (DataAccessException e) {
			response.put("HTTP", HttpURLConnection.HTTP_INTERNAL_ERROR);
			response.put("mensaje", "ERROR AL ELIMINAR EN LA BASE DE DATOS");
			response.put("error", e.getMostSpecificCause().getMessage());
		}
		return response;
	}

	@GetMapping("/api/usuarios/password")
	@ResponseBody
	public Boolean validateUserPassword(HttpServletResponse response,@RequestParam("password") String pass, @RequestParam("username") String user) {
		return usuarioService.isValidPassword(user, pass) ? Boolean.TRUE : Boolean.FALSE;
	}

	@PostMapping(path = "/api/usuarios/changePass")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public Map uptadePassword(@RequestBody PassFormDTO dto) {
		Map<String, Object> response = new HashMap<>();
		try {
			usuarioService.updatePasswordPersonal(dto.getUser(), dto.getPass(), dto.getPassConfirmed());
			response.put("HTTP", HttpURLConnection.HTTP_OK);
			response.put("mensaje", "Contraseña atualizada exitosamente:");
		} catch (DataAccessException e) {
			response.put("HTTP", HttpURLConnection.HTTP_INTERNAL_ERROR);
			response.put("mensaje", "ERROR");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
		}
		return response;
	}

	@PostMapping("/api/usuarios/passwordValidate")
	public Boolean acualPass(@RequestParam("password") String pass, @RequestParam("username") String user) {
		return usuarioService.isValidPassword(user, pass);
	}

	@GetMapping("/api/usuarios/roles")
	public DatatableWrapper<RolDTO> roles() {
		return new DatatableWrapper<>(
				rolService.obtenerTodo().stream().map(this::convertDTO).collect(Collectors.toList()));
	}

	private UsuarioDTO convertToDTO(Usuarios usuario) {
		return modelMapper.map(usuario, UsuarioDTO.class);
	}

	private RolDTO convertDTO(Roles roles) {
		return modelMapper.map(roles, RolDTO.class);
	}
}
