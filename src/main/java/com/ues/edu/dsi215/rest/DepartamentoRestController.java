package com.ues.edu.dsi215.rest;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ues.edu.dsi215.modelos.Departamento;
import com.ues.edu.dsi215.servicios.DepartamentoService;

@RestController
public class DepartamentoRestController {

	@Autowired
	private DepartamentoService service;

	@GetMapping("/api/Departamento/todos")
    public Map index() {
        Map<String, Object> response = new HashMap<>();
        response.put("data", service.obtenerTodo());
        return response;
    }

    @PostMapping("/api/Departamento/guardar")
    public Map guardar(@Valid @RequestBody Departamento departamento) {
        Map<String, Object> response = new HashMap<>();
        try {
            service.guardar(departamento);
            response.put("HTTP", HttpURLConnection.HTTP_OK);
            response.put("mensaje", "El Departamento :".concat(departamento.getNombre()).concat(" Ha sido guardado correctamente"));
        } catch (DataAccessException e) {
            response.put("HTTP", HttpURLConnection.HTTP_INTERNAL_ERROR);
            response.put("mensaje", "Error de validacion");
            response.put("error", e.getMostSpecificCause().getMessage());
        }
        return response;
    }

    @PutMapping("/api/Departamento/editar/{id}")
    public Map actualizar(@Valid @RequestBody Departamento departamento, @PathVariable Long id) {
        Map<String, Object> response = new HashMap<>();
        try {
            service.editar(departamento, id);
            response.put("HTTP", HttpURLConnection.HTTP_OK);
            response.put("mensaje", "El Departamento :".concat(departamento.getNombre()).concat(" Ha sido actualizado correctamente"));
        } catch (DataAccessException e) {
            response.put("HTTP", HttpURLConnection.HTTP_INTERNAL_ERROR);
            response.put("mensaje", "Error de validacion");
            response.put("error", e.getMostSpecificCause().getMessage());
        }
        return response;
    }
    @DeleteMapping("/api/Departamento/eliminar/{id}")
    public Map eliminar(@PathVariable Long id){
        Map<String, Object> response = new HashMap<>();
        try {
            service.eliminar(id);
             response.put("HTTP", HttpURLConnection.HTTP_OK);
            response.put("mensaje", " Departamento eliminado exitosamente");
        } catch (DataAccessException e) {
            response.put("HTTP", HttpURLConnection.HTTP_INTERNAL_ERROR);
            response.put("mensaje", "Error de validacion");
            response.put("error", e.getMostSpecificCause().getMessage());
        }
        return response;
    }

}
