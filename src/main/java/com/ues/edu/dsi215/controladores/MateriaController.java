package com.ues.edu.dsi215.controladores;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.ues.edu.dsi215.modelos.Materia;
import com.ues.edu.dsi215.servicios.DepartamentoService;

@Controller
public class MateriaController {

	@Autowired
	private DepartamentoService service;
	
	@GetMapping("/materia/index")
	public String departamentoIndex(Model model) {
		model.addAttribute("materia", new Materia());
		model.addAttribute("departamentos",service.obtenerTodo());
		return "administracion/Materia";
		
	}
}
