package com.ues.edu.dsi215.controladores;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.ues.edu.dsi215.modelos.Departamento;
import com.ues.edu.dsi215.servicios.JefaturaService;

@Controller
public class DepartamentoController {

	@Autowired
	private JefaturaService service;
	
	@GetMapping("/departamento/index")
	public String departamentoIndex(Model model) {
		model.addAttribute("departamento", new Departamento());
		model.addAttribute("jefaturas", service.obtenerTodo());
		return "administracion/Departamento";
		
	}
}
