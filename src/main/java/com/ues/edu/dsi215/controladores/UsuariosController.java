package com.ues.edu.dsi215.controladores;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.ues.edu.dsi215.modelos.Usuarios;
import com.ues.edu.dsi215.servicios.RolesService;

@Controller
public class UsuariosController {

	@Autowired
	private RolesService service;

	@GetMapping("/usuarios/index")
	public String usuariosIndex(Model model) {
		model.addAttribute("usuario", new Usuarios());
		model.addAttribute("roles", service.obtenerTodo());
		return "administracion/Usuarios";
	}
}
