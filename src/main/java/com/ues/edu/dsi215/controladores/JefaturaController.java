package com.ues.edu.dsi215.controladores;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.ues.edu.dsi215.modelos.Jefatura;


@Controller
public class JefaturaController {

	@GetMapping("/jefatura/index")
	public String departamentoIndex(Model model) {
		model.addAttribute("jefatura", new Jefatura());		
		return "administracion/Jefatura";
		
	}
}
