package com.ues.edu.dsi215.dto;

import java.time.LocalDate;
import java.util.Set;
import lombok.Data;

@Data
public class UsuarioDTO {

	private Long id;
	private String username;
	private String nombre;
	private String apellido;
	private String correo;
	private String cargo;
	private LocalDate joiningDate;
	private LocalDate lastLogin;
	private Set<RolDTO> roles;
}
