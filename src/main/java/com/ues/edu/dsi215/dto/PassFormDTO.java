package com.ues.edu.dsi215.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PassFormDTO {

	private String user;
	private String pass;
	private String passConfirmed;
}
