package com.ues.edu.dsi215.dto;

import java.util.List;

public class DatatableWrapper <T>{
    
    public DatatableWrapper(List<T> data) {
        this.data = data;
    }

    private List<T> data;

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }
}

