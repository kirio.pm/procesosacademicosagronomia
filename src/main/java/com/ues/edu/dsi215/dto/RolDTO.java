package com.ues.edu.dsi215.dto;

import lombok.Data;

@Data
public class RolDTO {

	private Long id;
	private String tipoRol;
}
