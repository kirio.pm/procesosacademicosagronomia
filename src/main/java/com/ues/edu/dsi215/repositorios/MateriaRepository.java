package com.ues.edu.dsi215.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ues.edu.dsi215.modelos.Materia;

public interface MateriaRepository extends JpaRepository<Materia,Long> {

	Materia findByNombre(String nombre);
}
