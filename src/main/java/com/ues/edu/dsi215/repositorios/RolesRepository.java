package com.ues.edu.dsi215.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ues.edu.dsi215.modelos.Roles;



public interface RolesRepository extends JpaRepository<Roles, Long>{

}
