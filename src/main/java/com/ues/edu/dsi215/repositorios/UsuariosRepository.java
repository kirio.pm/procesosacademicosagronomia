package com.ues.edu.dsi215.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ues.edu.dsi215.modelos.Usuarios;

public interface UsuariosRepository extends JpaRepository<Usuarios, Long> {
	Usuarios findByUsername(String username);

	Usuarios findByCorreo(String correo);

	@Modifying
	@Query("update Usuarios u set u.password = :password where u.id = :id")
	void updatePassword(@Param("password") String password, @Param("id") Long id);
}
