package com.ues.edu.dsi215.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ues.edu.dsi215.modelos.Departamento;

public interface DepartamentoRepository extends JpaRepository<Departamento,Long> {

	Departamento findByNombre(String nombre);
}
